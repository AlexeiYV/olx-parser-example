module.exports.queries = {
  GET_SINGLE_AD_URL: "SELECT url, user_id FROM ad_attrs LEFT JOIN ad_main ON ad_main.ad_id = ad_attrs.ad_id WHERE  ad_attrs.ad_id = (SELECT am.ad_id FROM ad_main am LEFT JOIN user_phone up ON up.user_id = am.user_id WHERE up.user_id IS NULL ORDER BY am.last_refreshed_time DESC LIMIT 1 )",
  SAVE_PHONE: "insert into user_phone SET ?"
}