const puppeteer = require('puppeteer');

module.exports.BrowserService = class BrowserService {
  async init() {
    try {
      this.browser = await puppeteer.launch({
        headless: true,
        args: ["--no-sandbox", "--disable-setuid-sandbox"],
      });
  
      this.page = await this.browser.newPage();
      await this.page.setUserAgent(
        "Mozilla/5.0 (Linux; Android 10; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Mobile Safari/537.36"
      );
    } catch(err) {
      console.log(err);
    }
  }
}