const mysql = require('mysql2');

module.exports.DBService = class DB {
  constructor(config, promisify) {
    console.log(config)
    this.pool = promisify ? mysql.createPool(config).promise() : mysql.createPool(config);
  }

  put(query, values) {
    console.log(query, values);
    return this.pool.query(query, values);
  }

  select(query, mapperFn) {
    console.log(query)
    const queryPromise = this.pool.execute(query);
    queryPromise.catch(err => console.log(err));
    if (mapperFn) return queryPromise.then(mapperFn);
    return queryPromise;
  }
}