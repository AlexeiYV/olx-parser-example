const mysql = require("mysql2");
const puppeteer = require("puppeteer");
const constants = require("./common/constants");
const { DBService } = require("./common/DBService");
const { BrowserService } = require('./common/BrowserService');
const { queries } = require("./common/queries");
const mappers = require("./common/mappers");

process.on("uncaughtException", function (err) {
  console.log(err);
});

const db = new DBService(
  {
    connectionLimit: constants.connectionLimit,
    host: constants.host,
    user: constants.user,
    password: constants.password,
    database: constants.database,
  },
  true
);

// logic:
// get page url from db
// parse with puppeteer
// insert phone into db

// repeat

const browser = new BrowserService();

(async () => {
  while(true) {
    await doTheJob();
  }
})();

function doTheJob() {
  return db.select(queries.GET_SINGLE_AD_URL, mappers.getAdInfo).then(async (result) => {
    try {
      await browser.init();
      await browser.page.goto(result.url);
      await browser.page.waitFor(2000);
      await browser.page.evaluate(async () => {
        window.scrollBy(0, 900);
      });
  
      await browser.page.waitFor(1000);
  
      await browser.page.click("button.css-1g5b3k2-BaseStyles");
  
      await browser.page.waitFor(1000);
      await browser.page.click(".css-su8w85-BaseStyles");
  
      await browser.page.waitFor(1000);
  
      let phones = await browser.page.evaluate(() => {
        let data = [];
        let elements = document.getElementsByClassName("css-v1ndtc");
        for (var element1 of elements) data.push(element1.textContent);
        return data;
      });
  
      let p_data = [];
      for (ph of phones) {
        // Clean numbers
        let p = ph
          .split(" ")
          .join("")
          .replace("+", "")
          .replace("-", "")
          .replace("-", "")
          .replace("-", "");
        let str_len = p.length;
  
        if (p.startsWith("0") & (str_len === 10)) {
          p = "38" + p;
        }
        if (str_len === 9) {
          p = "380" + p;
        }
  
        await db.put("insert into user_phone SET ?", {
          user_id: result.user_id,
          phone_id: p,
        });
      }
    } catch (e) {
      console.log(e);
    } finally {
      console.log("Exit");
      await browser.page.close();
      await browser.browser.close();
    }
  });  
}